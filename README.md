Développement en solo. Projet de Licence Pro Applications Web. 

Développement d'une application Android en Java, permettant de créer et lancer des programmes d'entraînement sportif. 
Gestion d'un chrono, du stockage local, de la pile de l'application, de l'interface mobile.

Pour tester l'application :

- Télécharger les fichiers
- Ouvrir le projet avec Android Studio
- Lancer