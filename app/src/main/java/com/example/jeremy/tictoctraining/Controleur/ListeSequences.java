package com.example.jeremy.tictoctraining.Controleur;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jeremy.tictoctraining.Modele.DatabaseClient;
import com.example.jeremy.tictoctraining.Modele.Sequence;
import com.example.jeremy.tictoctraining.R;

import java.util.List;

/**
 * Activite principale =>
 * Affichage de la liste des sequences de l'utilisateur /
 * Possibilite d'ajout de sequence
 */
public class ListeSequences extends AppCompatActivity {
    public static final String ID_SEQUENCE = "idSequence";
    private DatabaseClient mDb;
    private List<Sequence> sequences;

    /**
     * Creation de l'activite
     * Recuperation en BD et affichage des sequences existantes
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_sequences);
        // Récupération du DatabaseClient
        mDb = DatabaseClient.getInstance(getApplicationContext());

        class addSequenceTask extends AsyncTask<Void, Void, List<Sequence>> {

            @Override
            protected List<Sequence> doInBackground(Void... voids) {
                // Récupération des séquences en BD
                sequences = mDb.getAppDatabase().SequenceDao().getAll();
                return sequences;
            }

            @Override
            protected void onPostExecute(List<Sequence> sequences) {
                super.onPostExecute(sequences);
                // Affichage des séquences
                for (Sequence sequence : sequences) {
                    creerVueSequence(sequence.getId(), sequence.getLibelle());
                }

            }
        }
        // Création d'un objet de type GetTasks et execution de la demande asynchrone
        addSequenceTask aDB = new addSequenceTask();
        aDB.execute();
    }

    /**
     * Utilisation du bouton AJOUTER SEQUENCE =>
     * Creation d'une nouvelle sequence
     * @param view
     */
    public void ajouteSequence(View view) {
        class addSequenceTask extends AsyncTask<Void, Void, Sequence> {
            private String idSequence;

            @Override
            protected Sequence doInBackground(Void... voids) {
                // Création de la séquence et insertion en BD
                Sequence newSequence = new Sequence();
                idSequence = Long.toString(mDb.getAppDatabase().SequenceDao().insert(newSequence));
                return newSequence;
            }

            @Override
            protected void onPostExecute(Sequence sequence) {
                super.onPostExecute(sequence);
                // Création de la vue de la séquence
                creerVueSequence((int)Integer.valueOf(idSequence),"Nouvelle séquence");
            }
        }
        // Création d'un objet de type GetTasks et execution de la demande asynchrone
        addSequenceTask aDB = new addSequenceTask();
        aDB.execute();

    }

    /**
     * Utilisation du bouton EDITER d'une sequence =>
     * Passage a l'activite d'edition de cette sequence
     * @param view
     */
    public void editerSequence(View view) {
        Intent intent = new Intent(this, EditeSequence.class);
        intent.putExtra(ID_SEQUENCE, view.getTag().toString());

        // Si retour à cette activité, termine toutes les activités au-dessus dans la pile
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    /**
     * Utilisation du bouton SUPPRIMER d'une sequence =>
     * Suppression de cette sequence
     * @param view
     */
    public void supprimerSequence(final View view) {
        final int idSequence = (int)Integer.valueOf(view.getTag().toString());
        class addSequenceTask extends AsyncTask<Void, Void, Sequence> {

            @Override
            protected Sequence doInBackground(Void... voids) {
                // Identification et suppression de la séquence en BD
                Sequence sequenceASupp = mDb.getAppDatabase().SequenceDao().getById(idSequence);
                mDb.getAppDatabase().SequenceDao().delete(sequenceASupp);
                return sequenceASupp;
            }

            @Override
            protected void onPostExecute(Sequence sequence) {
                super.onPostExecute(sequence);
            }
        }
        // Création d'un objet de type GetTasks et execution de la demande asynchrone
        addSequenceTask aDB = new addSequenceTask();
        aDB.execute();
        // Rafraichissement de l'activité
        finish();
        startActivity(getIntent());
    }

    /**
     * Utilisation du bouton JOUER d'une sequence =>
     * Passage a l'activite de lecture de cette sequence
     * @param view
     */
    public void jouerSequence(View view) {
        Intent intent = new Intent(this, JoueSequence.class);
        intent.putExtra(ID_SEQUENCE, view.getTag().toString());

        // Si retour à cette activité, termine toutes les activités au-dessus dans la pile
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    /**
     * Creation de la vue pour une sequence donnee, avec libelle au choix
     * @param idSequence
     * @param libelle
     */
    private void creerVueSequence(int idSequence, String libelle) {
        String idSequenceString = String.valueOf(idSequence);

        final LinearLayout layout = (LinearLayout) findViewById(R.id.sequencesContainer);
        final CardView cardSequence = new CardView(this);
        final LinearLayout layoutCardSequence = new LinearLayout(this);
        final TextView txtLibelle = new TextView(this);
        final Button btnEditer = new Button(this);
        final Button btnSupprimer = new Button(this);
        final Button btnJouer = new Button(this);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(10,20,10,20);
        cardSequence.setLayoutParams(layoutParams);

        layoutCardSequence.setTag(idSequenceString);
        layoutCardSequence.setOrientation(LinearLayout.VERTICAL);
        layoutCardSequence.setPadding(20,20,20,20);


        btnEditer.setTag(idSequenceString);
        btnEditer.setText("Editer");
        btnEditer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                editerSequence(view1);
            }
        });

        btnSupprimer.setTag(idSequenceString);
        btnSupprimer.setText("Supprimer");
        btnSupprimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                supprimerSequence(view1);
            }
        });

        btnJouer.setTag(idSequenceString);
        btnJouer.setText("Jouer");
        btnJouer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                jouerSequence(view1);
            }
        });

        txtLibelle.setTextSize(24);
        txtLibelle.setText(libelle);

        layoutCardSequence.addView(txtLibelle);
        layoutCardSequence.addView(btnEditer);
        layoutCardSequence.addView(btnSupprimer);
        layoutCardSequence.addView(btnJouer);

        cardSequence.addView(layoutCardSequence);
        layout.addView(cardSequence);
    }
}
