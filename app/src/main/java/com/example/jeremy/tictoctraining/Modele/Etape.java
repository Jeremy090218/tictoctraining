package com.example.jeremy.tictoctraining.Modele;

/**
 * Classe d'enumeration des differentes etapes possibles dans une sequence
 */
public enum Etape {
    LIBELLE,
    TEMPS_PREPA,
    TEMPS_TRAVAIL,
    TEMPS_REPOS_C,
    TEMPS_REPOS_L,
    NB_CYCLES,
    NB_SERIES
}
