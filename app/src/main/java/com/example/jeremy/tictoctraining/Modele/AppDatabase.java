package com.example.jeremy.tictoctraining.Modele;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Sequence.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract SequenceDao SequenceDao();
}
