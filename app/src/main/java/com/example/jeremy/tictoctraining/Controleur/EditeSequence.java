package com.example.jeremy.tictoctraining.Controleur;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeremy.tictoctraining.Modele.DatabaseClient;
import com.example.jeremy.tictoctraining.Modele.Etape;
import com.example.jeremy.tictoctraining.Modele.Sequence;
import com.example.jeremy.tictoctraining.R;

import java.util.ArrayList;

import static com.example.jeremy.tictoctraining.Modele.Etape.LIBELLE;
import static com.example.jeremy.tictoctraining.Modele.Etape.NB_CYCLES;
import static com.example.jeremy.tictoctraining.Modele.Etape.NB_SERIES;
import static com.example.jeremy.tictoctraining.Modele.Etape.TEMPS_PREPA;
import static com.example.jeremy.tictoctraining.Modele.Etape.TEMPS_REPOS_C;
import static com.example.jeremy.tictoctraining.Modele.Etape.TEMPS_REPOS_L;
import static com.example.jeremy.tictoctraining.Modele.Etape.TEMPS_TRAVAIL;

/**
 * Activite d'edition d'une sequence
 */
public class EditeSequence extends AppCompatActivity {
    private DatabaseClient mDb;
    private String idSequence;
    private Sequence sequence;
    private ArrayList<EditText> editTexts;

    /**
     * Creation de l'activite
     * Recuperation en BD et affichage des attributs de la sequence sous forme de champs modifiables
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edite_sequence);

        // Création d'un champs de texte pour l'affichage/édition du libellé de la séquence
        final EditText txtLibelle = new EditText(this);
        // Récupération du layout accueillant les attributs
        final LinearLayout layout = (android.widget.LinearLayout) findViewById(R.id.attributsContainer);

        // Création d'une liste accueillant les champs de saisie de chaque attribut
        editTexts = new ArrayList<>();

        // Récupération du DatabaseClient
        mDb = DatabaseClient.getInstance(getApplicationContext());
        // Récupération de l'id de la séquence à éditer
        idSequence = getIntent().getExtras().getString("idSequence");


        class addSequenceTask extends AsyncTask<Void, Void, Sequence> {

            @Override
            protected Sequence doInBackground(Void... voids) {
                // Récupération de la séquence à éditer
                sequence = mDb.getAppDatabase().SequenceDao().getById((int) Integer.valueOf(idSequence));
                return sequence;
            }

            @Override
            protected void onPostExecute(Sequence sequence) {
                super.onPostExecute(sequence);
                // Modification visuelle du champs de saisie du libellé de la séquence
                txtLibelle.setText(sequence.getLibelle());
                txtLibelle.setTextColor(getResources().getColor(R.color.colorAccent));
                txtLibelle.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                layout.addView(txtLibelle);
                // Ajout d'un tag pour identification lors de l'ajout en BD après validation
                txtLibelle.setTag(LIBELLE);


                // Création d'un éditeur complet pour chacun des attributs
                createAttributeEditor("Temps de préparation", sequence.getTempsPreparation(), TEMPS_PREPA);
                createAttributeEditor("Temps de travail", sequence.getTempsTravail(), TEMPS_TRAVAIL);
                createAttributeEditor("Temps de repos court", sequence.getTempsReposCourt(), TEMPS_REPOS_C);
                createAttributeEditor("Temps de repos long", sequence.getTempsReposLong(), TEMPS_REPOS_L);
                createAttributeEditor("Nombre de cycles", sequence.getNbCycles(), NB_CYCLES);
                createAttributeEditor("Nombre de séries", sequence.getNbSeries(), NB_SERIES);

                // Stockage du champs de saisie contenant le libellé de la séquence
                editTexts.add(txtLibelle);
            }
        }

        // Création d'un objet de type GetTasks et execution de la demande asynchrone
        addSequenceTask aDB = new addSequenceTask();
        aDB.execute();
    }

    /**
     * Creation d'un editeur d'attribut complet pour un attribut donne
     * @param libelle
     * @param valeur
     * @param attributEnum
     */
    private void createAttributeEditor(String libelle, int valeur, Etape attributEnum) {
        String attribut = attributEnum.toString();
        LinearLayout layout = (android.widget.LinearLayout) findViewById(R.id.attributsContainer);
        CardView cardAttribut = new CardView(this);
        LinearLayout layoutCardAttribut = new LinearLayout(this);

        TextView attLibelle = new TextView(this);
        Button btnDiminuer = new Button(this);
        final EditText editAttribute = new EditText(this);
        Button btnAugmenter = new Button(this);

        layoutCardAttribut.setOrientation(LinearLayout.HORIZONTAL);
        layoutCardAttribut.setGravity(View.TEXT_ALIGNMENT_CENTER);
        attLibelle.setText(libelle);
        attLibelle.setWidth(270);
        attLibelle.setPadding(10, 0, 0, 0);
        btnDiminuer.setTag(attribut);
        btnDiminuer.setText("-");
        btnDiminuer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                diminuerAttribut(view1);
            }
        });

        editAttribute.setTag(attribut);
        editAttribute.setWidth(80);
        editAttribute.setText(String.valueOf(valeur));
        editAttribute.setInputType(InputType.TYPE_CLASS_NUMBER);
        btnAugmenter.setTag(attribut);
        btnAugmenter.setText("+");
        btnAugmenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                augmenterAttribut(view1);
            }
        });

        layoutCardAttribut.addView(attLibelle);
        layoutCardAttribut.addView(btnDiminuer);
        layoutCardAttribut.addView(editAttribute);
        layoutCardAttribut.addView(btnAugmenter);
        cardAttribut.addView(layoutCardAttribut);
        layout.addView(cardAttribut);
        editTexts.add(editAttribute);
    }

    /**
     * Utilisation d'un bouton "-" =>
     * Diminution de l'attribut correspondant
     *
     * @param view
     */
    public void diminuerAttribut(View view) {
        for (EditText editAttribute : editTexts) {
            if (editAttribute.getTag() == view.getTag()) {
                editAttribute.setText(String.valueOf((int) Integer.valueOf(editAttribute.getText().toString()) - 1));
            }
        }
    }

    /**
     * Utilisation d'un bouton "+" =>
     * Augmentation de l'attribut correspondant
     *
     * @param view
     */
    public void augmenterAttribut(View view) {
        for (EditText editAttribute : editTexts) {
            if (editAttribute.getTag() == view.getTag()) {
                editAttribute.setText(String.valueOf((int) Integer.valueOf(editAttribute.getText().toString()) + 1));
            }
        }
    }

    /**
     * Utilisation du bouton VALIDER =>
     * Sauvegarde en BD des modifications et retour a la liste des sequences
     *
     * @param view
     */
    public void validerEdition(View view) {
        class addSequenceTask extends AsyncTask<Void, Void, Sequence> {

            @Override
            protected Sequence doInBackground(Void... voids) {
                for (EditText editAttribute : editTexts) {
                    // Identification de chaque attribut grâce au tag de leur champs de saisie respectif
                    // Puis mise à jour de la valeur de l'attribut en BD
                    switch (editAttribute.getTag().toString()) {
                        case "TEMPS_PREPA":
                            sequence.setTempsPreparation((int) Integer.valueOf(editAttribute.getText().toString()));
                            break;
                        case "TEMPS_TRAVAIL":
                            sequence.setTempsTravail((int) Integer.valueOf(editAttribute.getText().toString()));
                            break;
                        case "TEMPS_REPOS_C":
                            sequence.setTempsReposCourt((int) Integer.valueOf(editAttribute.getText().toString()));
                            break;
                        case "TEMPS_REPOS_L":
                            sequence.setTempsReposLong((int) Integer.valueOf(editAttribute.getText().toString()));
                            break;
                        case "NB_CYCLES":
                            sequence.setNbCycles((int) Integer.valueOf(editAttribute.getText().toString()));
                            break;
                        case "NB_SERIES":
                            sequence.setNbSeries((int) Integer.valueOf(editAttribute.getText().toString()));
                            break;
                        case "LIBELLE":
                            sequence.setLibelle(editAttribute.getText().toString());
                            break;
                    }
                    mDb.getAppDatabase().SequenceDao().update(sequence);
                }
                return sequence;
            }

            @Override
            protected void onPostExecute(Sequence sequence) {
                super.onPostExecute(sequence);
                Toast toast = Toast.makeText(getApplicationContext(), "Enregistrement réussi !", Toast.LENGTH_SHORT);
                toast.show();
            }
        }

        // Création d'un objet de type GetTasks et execution de la demande asynchrone
        addSequenceTask aDB = new addSequenceTask();
        aDB.execute();
        Intent intent = new Intent(this, ListeSequences.class);
        startActivity(intent);
    }

    /**
     * Utilisation du bouton RETOUR MENU =>
     * Retour menu sans sauvegarde
     *
     * @param view
     */
    public void retourMenu(View view) {
        Intent intent = new Intent(this, ListeSequences.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    /**
     * Empeche le retour via le bouton retour du telephone
     * Evite une erreur de l'utilisateur (perte des données inserees par retour involontaire)
     */
    @Override
    public void onBackPressed() {
        Toast toast = Toast.makeText(getApplicationContext(), "Utilisez un bouton pour quitter", Toast.LENGTH_LONG);
        toast.show();
    }
}
