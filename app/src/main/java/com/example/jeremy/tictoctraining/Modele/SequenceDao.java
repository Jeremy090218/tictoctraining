package com.example.jeremy.tictoctraining.Modele;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Interface d'acces aux Sequences stockees en BD
 */
@Dao
public interface SequenceDao {
    @Query("SELECT * FROM sequences")
    List<Sequence> getAll();

    @Query("SELECT * FROM sequences WHERE id = (:sequenceId)")
    Sequence getById(int sequenceId);

    @Query("SELECT * FROM sequences WHERE id IN (:sequencesIds)")
    List<Sequence> loadAllByIds(int[] sequencesIds);

    @Insert
    long insert(Sequence sequence);

    @Delete
    void delete(Sequence sequence);

    @Update
    void update(Sequence sequence);
}

