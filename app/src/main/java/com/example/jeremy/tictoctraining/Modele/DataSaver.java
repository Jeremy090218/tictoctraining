package com.example.jeremy.tictoctraining.Modele;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Classe Parcelable de sauvegarde des données lors du retournement de l'écran
 */
public class DataSaver implements Parcelable {
    private int nbSeriesRestantes;
    private int nbCyclesRestants;
    private Etape etapeActuelle;
    private long updatedTime;
    private Boolean isRunning;

    /**
     * Constructeur pour sauvegarde des données
     */
    public DataSaver(int nbSeriesRestantes,
                     int nbCyclesRestants,
                     Etape etapeActuelle,
                     long updatedTime,
                     Boolean isRunning) {
        this.nbSeriesRestantes = nbSeriesRestantes;
        this.nbCyclesRestants = nbCyclesRestants;
        this.etapeActuelle = etapeActuelle;
        this.updatedTime = updatedTime;
        this.isRunning = isRunning;
    }

    /**
     * Constructeur pour lecture des données sauvegardées
     */
    public DataSaver(Parcel parcel) {
        nbSeriesRestantes = parcel.readInt();
        nbCyclesRestants = parcel.readInt();
        etapeActuelle = Etape.valueOf(parcel.readString());
        updatedTime = parcel.readInt();
        isRunning = Boolean.valueOf(parcel.readString());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(nbSeriesRestantes);
        dest.writeInt(nbCyclesRestants);
        dest.writeString(etapeActuelle.toString());
        dest.writeLong(updatedTime);
        dest.writeString(isRunning.toString());
    }

    public static final Parcelable.Creator<DataSaver> CREATOR = new Parcelable.Creator<DataSaver>(){

        @Override
        public DataSaver createFromParcel(Parcel parcel) {
            return new DataSaver(parcel);
        }

        @Override
        public DataSaver[] newArray(int size) {
            return new DataSaver[0];
        }
    };


    public int getNbSeriesRestantes() {
        return nbSeriesRestantes;
    }

    public int getNbCyclesRestants() {
        return nbCyclesRestants;
    }

    public Etape getEtapeActuelle() {
        return etapeActuelle;
    }

    public long getUpdatedTime() {
        return updatedTime;
    }

    public Boolean getRunning() {
        return isRunning;
    }
}
