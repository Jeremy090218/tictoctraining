package com.example.jeremy.tictoctraining.Controleur;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeremy.tictoctraining.Modele.DataSaver;
import com.example.jeremy.tictoctraining.Modele.DatabaseClient;
import com.example.jeremy.tictoctraining.Modele.Etape;
import com.example.jeremy.tictoctraining.Modele.Sequence;
import com.example.jeremy.tictoctraining.R;

/**
 * Activite de lecture avec compteur d'une sequence
 */
public class JoueSequence extends AppCompatActivity {
    // PARAMETRES SEQUENCE
    private DatabaseClient mDb;
    private String idSequence;
    private Sequence sequence;
    private int nbCyclesTotal;
    private int nbCyclesRestants;
    private int nbSeriesRestantes;
    private Etape etapeActuelle;

    // CONSTANTE
    private final static long INITIAL_TIME = 5000;

    // VIEW
    private LinearLayout fondSequence;
    private LinearLayout layoutBoutons;
    private Button startButton;
    private Button pauseButton;
    private Button retourButton;
    private TextView timerValue;
    private TextView etapeSequence;
    private TextView nbSeriesView;
    private TextView nbCyclesView;

    // DATA
    private long updatedTime = INITIAL_TIME;
    private CountDownTimer timer;
    private DataSaver savedData;
    private Boolean isRunning;

    // SOUND
    private MediaPlayer ticMedia;
    private MediaPlayer dingMedia;
    private MediaPlayer applauseMedia;

    /**
     * Creation de l'activite
     * Recuperation en BD et affichage du compteur correspondant à la sequence choisie
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joue_sequence);
        // Instanciation des sons
        ticMedia = MediaPlayer.create(getApplicationContext(), R.raw.tic);
        dingMedia = MediaPlayer.create(getApplicationContext(), R.raw.ding);
        applauseMedia = MediaPlayer.create(getApplicationContext(), R.raw.applause);


        // Récupérer les views
        fondSequence = (LinearLayout) findViewById(R.id.fondSequence);
        nbSeriesView = (TextView) findViewById(R.id.nbSeriesView);
        nbCyclesView = (TextView) findViewById(R.id.nbCyclesView);
        etapeSequence = (TextView) findViewById(R.id.etapeSequence);
        timerValue = (TextView) findViewById(R.id.timerValue);
        layoutBoutons = (LinearLayout) findViewById(R.id.layoutBoutons);
        startButton = (Button) findViewById(R.id.startButton);
        pauseButton = (Button) findViewById(R.id.pauseButton);
        retourButton = (Button) findViewById(R.id.retourButton);

        // Récupération du DatabaseClient
        mDb = DatabaseClient.getInstance(getApplicationContext());
        // Récupération de l'id de la séquence à éditer
        idSequence = getIntent().getExtras().getString("idSequence");

        class addSequenceTask extends AsyncTask<Void, Void, Sequence> {

            @Override
            protected Sequence doInBackground(Void... voids) {
                // Récupération de la séquence à éditer
                sequence = mDb.getAppDatabase().SequenceDao().getById((int)Integer.valueOf(idSequence));
                return sequence;
            }

            @Override
            protected void onPostExecute(Sequence sequence) {
                super.onPostExecute(sequence);
                // Si il y a eu retournement de l'écran, récupération de l'état
                if (savedInstanceState != null) {
                    nbSeriesRestantes = savedData.getNbSeriesRestantes();
                    nbCyclesRestants = savedData.getNbCyclesRestants();
                    etapeActuelle = savedData.getEtapeActuelle();
                    updatedTime = savedData.getUpdatedTime();
                    isRunning = savedData.getRunning();

                    if (etapeActuelle == Etape.TEMPS_TRAVAIL) {
                        fondSequence.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                        etapeSequence.setText("Travail");
                    } else {
                        fondSequence.setBackgroundColor(getResources().getColor(R.color.colorRed));
                        if (etapeActuelle == Etape.TEMPS_PREPA) {
                            etapeSequence.setText("Préparation");
                        } else if (etapeActuelle == Etape.TEMPS_REPOS_C) {
                            etapeSequence.setText("Repos Court");
                        } else {
                            etapeSequence.setText("Repos Long");
                        }
                    }
                    if (isRunning) {
                        startButton.setVisibility(View.INVISIBLE);
                        start();
                    } else {
                        pauseButton.setVisibility(View.INVISIBLE);
                    }
                // Sinon initialisation par défaut
                } else {
                    fondSequence.setBackgroundColor(getResources().getColor(R.color.colorRed));
                    pauseButton.setVisibility(View.INVISIBLE);

                    nbCyclesRestants = sequence.getNbCycles();
                    nbSeriesRestantes = sequence.getNbSeries();
                    etapeActuelle = Etape.TEMPS_PREPA;
                    updatedTime = (long)sequence.getTempsPreparation()*1000;
                    etapeSequence.setText("Préparation");
                    isRunning = false;
                }
                // Initialisations communes aux deux cas
                layoutBoutons.setBackgroundColor(getResources().getColor(R.color.colorGray));
                pauseButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                startButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                nbSeriesView.setText(" " + String.valueOf(nbSeriesRestantes));
                nbCyclesView.setText(" " + String.valueOf(nbCyclesRestants));
                nbCyclesTotal = sequence.getNbCycles();
                retourButton.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                miseAJour();
            }
        }

        // Création d'un objet de type GetTasks et execution de la demande asynchrone
        addSequenceTask aDB = new addSequenceTask();
        aDB.execute();
    }

    /**
     * Utilisation du bouton START =>
     * Lancement du compteur
     * @param view
     */
    public void onStart(View view) {
        start();
        pauseButton.setVisibility(View.VISIBLE);
        startButton.setVisibility(View.INVISIBLE);
    }

    /**
     * Lancement du compteur
     */
    private void start() {
        timer = new CountDownTimer(updatedTime, 10) {
            public void onTick(long millisUntilFinished) {
                updatedTime = millisUntilFinished;
                miseAJour();
            }
            public void onFinish() {
                updatedTime = 0;
                miseAJour();
            }
        }.start();
        isRunning = true;
    }

    /**
     * Utilisation du bouton PAUSE =>
     * Mise en pause du compteur
     * @param view
     */
    public void onPause(View view) {
        if (timer != null) {
            timer.cancel(); // Arrete le CountDownTimer
        }
        pauseButton.setVisibility(View.INVISIBLE);
        startButton.setVisibility(View.VISIBLE);
        isRunning = false;
    }


    /**
     * Mise à jour graphique du compteur et changement d'etape lorsque le compteur est à zero
     */
    private void miseAJour() {
        // Fin d'une étape, analyse du statut actuel et passage à l'étape suivante
        if (updatedTime == 0) {
            dingMedia.start();
            if (etapeActuelle == Etape.TEMPS_PREPA || etapeActuelle == Etape.TEMPS_REPOS_C) {
                finCycle();
                start();
            } else if (etapeActuelle == Etape.TEMPS_TRAVAIL) {
                fondSequence.setBackgroundColor(getResources().getColor(R.color.colorRed));
                etapeActuelle = Etape.TEMPS_REPOS_C;
                updatedTime = (long)sequence.getTempsReposCourt()*1000;
                etapeSequence.setText("Repos Court");
                miseAJour();
                start();
            } else if (etapeActuelle == Etape.TEMPS_REPOS_L) {
                finSerie();
            }
        }
        // Décompositions en secondes et minutes
        int secs = (int) (updatedTime / 1000);
        int mins = secs / 60;
        secs = secs % 60;
        int milliseconds = (int) (updatedTime % 1000);

        if (milliseconds < 100) {
            ticMedia.start();
        }
        // Affichage du "timer"
        timerValue.setText("" + mins + ":"
                + String.format("%02d", secs) + ":"
                + String.format("%03d", milliseconds));

    }

    /**
     * Determination de l'etape suivant la fin d'un cycle
     * Si d'autres cycles restent à passer, lancement du cycle suivant
     * Sinon lancement du repos long
     */
    private void finCycle() {
        if (nbCyclesRestants > 0) {
            etapeActuelle = Etape.TEMPS_TRAVAIL;
            fondSequence.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            updatedTime = (long)sequence.getTempsTravail()*1000;
            etapeSequence.setText("Travail");
            nbCyclesRestants -= 1;
            nbCyclesView.setText(String.valueOf(" " + nbCyclesRestants));
            miseAJour();
        } else {
            etapeActuelle = Etape.TEMPS_REPOS_L;
            updatedTime = (long)sequence.getTempsReposLong()*1000;
            etapeSequence.setText("Repos Long");
            miseAJour();
        }
    }

    /**
     * Determination de l'etape suivant la fin d'une serie
     * Si d'autres series restent à passer, lancement de la serie suivante
     * Sinon fin de sequence
     */
    private void finSerie() {
        if (nbSeriesRestantes > 0) {
            etapeActuelle = Etape.TEMPS_TRAVAIL;
            fondSequence.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            updatedTime = (long)sequence.getTempsTravail()*1000;
            etapeSequence.setText("Travail");
            nbCyclesRestants = nbCyclesTotal;
            nbCyclesView.setText(String.valueOf(" " + nbCyclesRestants));
            nbSeriesRestantes -= 1;
            nbSeriesView.setText(String.valueOf(" " + nbSeriesRestantes));
            miseAJour();
            start();
        } else {
            etapeSequence.setText("Séquence terminée !");
            applauseMedia.start();
        }
    }

    /**
     * Utilisation du bouton RETOUR MENU =>
     * Retour au menu d'affichage des sequences
     * @param view
     */
    public void retourMenu(View view) {
        Intent intent = new Intent(this,ListeSequences.class);
        startActivity(intent);
        if (timer != null) {
            timer.cancel(); // Arrete le CountDownTimer
        }
        finish();
    }

    /**
     * Sauvegarde des donnees lors du retournement de l'ecran
     * @param outState
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (timer != null) {
            timer.cancel(); // Arrete le CountDownTimer
        }
        savedData = new DataSaver(nbSeriesRestantes,nbCyclesRestants,etapeActuelle,updatedTime, isRunning);
        outState.putParcelable("savedData",savedData);
    }

    /**
     * Recuperation des donnees apres le retournement de l'ecran
     * @param savedInstanceState
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        savedData = savedInstanceState.getParcelable("savedData");
        super.onRestoreInstanceState(savedInstanceState);
    }

    /**
     * Empeche le retour via le bouton retour du téléphone
     * Evite une erreur de l'utilisateur (perte de l'avancement de l'entrainement par retour involontaire)
     */
    @Override
    public void onBackPressed() {
        Toast toast = Toast.makeText(getApplicationContext(), "Utilisez le bouton Retour Menu pour quitter", Toast.LENGTH_LONG);
        toast.show();
    }
}

