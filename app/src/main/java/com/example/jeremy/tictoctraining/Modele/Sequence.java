package com.example.jeremy.tictoctraining.Modele;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.util.SparseArray;

import java.util.HashMap;

/**
 * Classe dzterminant une szquence d'entrainement
 */
@Entity(tableName = "sequences",
        indices = {@Index(value = {"libelle", "tempsPreparation", "tempsTravail",
                "tempsReposCourt", "tempsReposLong",
                "nbCycles", "nbSeries"})})
public class Sequence {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "libelle")
    private String libelle;
    @ColumnInfo(name = "tempsPreparation")
    private int tempsPreparation;
    @ColumnInfo(name = "tempsTravail")
    private int tempsTravail;
    @ColumnInfo(name = "tempsReposCourt")
    private int tempsReposCourt;
    @ColumnInfo(name = "tempsReposLong")
    private int tempsReposLong;
    @ColumnInfo(name = "nbCycles")
    private int nbCycles;
    @ColumnInfo(name = "nbSeries")
    private int nbSeries;

    public Sequence() {
        libelle = "Nouvelle séquence";
        tempsPreparation = 60;
        tempsTravail = 30;
        tempsReposCourt = 60;
        tempsReposLong = 180;
        nbCycles = 10;
        nbSeries = 2;
    }

    public int getId() {
        return id;
    }

    public String getLibelle() { return libelle; }

    public int getNbCycles() {
        return nbCycles;
    }

    public int getNbSeries() {
        return nbSeries;
    }

    public int getTempsTravail() { return tempsTravail; }

    public int getTempsPreparation() {
        return tempsPreparation;
    }

    public int getTempsReposCourt() {
        return tempsReposCourt;
    }

    public int getTempsReposLong() {
        return tempsReposLong;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLibelle(String libelle) { this.libelle = libelle; }

    public void setTempsPreparation(int tempsPreparation) {
        this.tempsPreparation = tempsPreparation;
    }

    public void setTempsTravail(int tempsTravail) { this.tempsTravail = tempsTravail; }

    public void setNbCycles(int nbCycles) {
        this.nbCycles = nbCycles;
    }

    public void setNbSeries(int nbSeries) {
        this.nbSeries = nbSeries;
    }

    public void setTempsReposCourt(int tempsReposCourt) {
        this.tempsReposCourt = tempsReposCourt;
    }

    public void setTempsReposLong(int tempsReposLong) {
        this.tempsReposLong = tempsReposLong;
    }
}
